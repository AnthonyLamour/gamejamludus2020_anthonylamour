﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadOnClick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Button>().onClick.AddListener(ClickLoad);
    }

    // ClickLoad
    void ClickLoad()
    {
        //lancement de la fonction
        this.GetComponent<LoadScene>().LoadMyScene();
    }
}
