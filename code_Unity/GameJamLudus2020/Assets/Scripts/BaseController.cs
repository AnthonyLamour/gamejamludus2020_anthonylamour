﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BaseController : MonoBehaviour
{
    public Sprite deadBase;
    public Sprite aliveBase;
    public int maxLife;
    public GameObject quiz;

    private string baseState;
    private GameObject gameController;
    private GameObject theQuiz;
    private int currentLife;

    public float coolDown;
    private float nextDmg;

    private void Start()
    {
        baseState = "mauvais";
        transform.GetChild(0).GetComponent<Image>().sprite = deadBase;
        this.gameObject.GetComponent<Button>().onClick.AddListener(RepairBase);
        gameController = GameObject.FindGameObjectWithTag("GameController");
        currentLife = maxLife / 2;
    }

    private void RepairBase()
    {
        if (baseState == "mauvais")
        {

            gameController.GetComponent<Game_Controller>().SetGameState("pause");
            theQuiz=Instantiate(quiz,transform.parent.transform);
            theQuiz.transform.GetChild(0).gameObject.GetComponent<QuestionController>().SetCallingObject(this.gameObject);
        }
    }

    public void SendReponse(bool bonneReponse)
    {
        if (bonneReponse)
        {
            baseState = "bon";
            transform.GetChild(0).GetComponent<Image>().sprite = aliveBase;
            currentLife = maxLife;
            this.GetComponent<BoxCollider2D>().isTrigger = false;
        }
        gameController.GetComponent<Game_Controller>().SetGameState("runing");
    }

    public int GetCurrentLife()
    {
        return currentLife;
    }

    public int GetMaxLife()
    {
        return maxLife;
    }

    public void SetCurrentLife(int newCurrentLife)
    {
        currentLife = newCurrentLife;
    }

    public void SetMaxLife(int newMaxLife)
    {
        maxLife = newMaxLife;
    }

    public void TakeDmg(int dmg)
    {
        if (Time.time > nextDmg)
        {
            nextDmg = Time.time + coolDown;
            if (currentLife - dmg > 0)
            {
                currentLife = currentLife - dmg;
            }
            else
            {
                currentLife = 0;
                transform.GetChild(0).GetComponent<Image>().sprite = deadBase;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
                baseState = "dead";
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

}
