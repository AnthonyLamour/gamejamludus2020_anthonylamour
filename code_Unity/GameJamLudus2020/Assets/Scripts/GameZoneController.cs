﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameZoneController : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //récupération du recttransform de la gameZone
        var gameZoneRect = this.GetComponent<RectTransform>();
        //récupération de la collider 2D de la gameZone
        var gameZoneCollider = this.GetComponent<BoxCollider2D>();

        //reset de l'offset de la collider
        gameZoneCollider.offset = new Vector2(0, 0);
        //resize de la collider
        gameZoneCollider.size = new Vector2(gameZoneRect.rect.width,gameZoneRect.rect.height);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
    }
}
