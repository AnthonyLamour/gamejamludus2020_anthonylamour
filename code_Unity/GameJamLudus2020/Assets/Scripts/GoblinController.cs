﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoblinController : MonoBehaviour
{ 

    public int maxLife;
    public GameObject quiz;
    public int dmg;

    private string goblinState;
    private GameObject gameController;
    private GameObject theQuiz;
    private GameObject goblinImage;
    private int currentLife;

    public float coolDown;
    private float nextDmg;
    

    private void Start()
    {
        goblinState = "mauvais";
        this.gameObject.GetComponent<Button>().onClick.AddListener(RepairBase);
        gameController = GameObject.FindGameObjectWithTag("GameController");
        goblinImage = transform.GetChild(0).gameObject;
        goblinImage.GetComponent<Animator>().SetBool("isAlive", false);
        this.GetComponent<BoxCollider2D>().isTrigger = true;
        currentLife = 0;
    }

    private void RepairBase()
    {
        if (goblinState == "mauvais")
        {

            gameController.GetComponent<Game_Controller>().SetGameState("pause");
            theQuiz = Instantiate(quiz, transform.parent.transform);
            theQuiz.transform.GetChild(0).gameObject.GetComponent<QuestionController>().SetCallingObject(this.gameObject);
        }
        
    }

    public void SendReponse(bool bonneReponse)
    {
        if (bonneReponse)
        {
            goblinState = "bon";
            goblinImage.GetComponent<Animator>().SetBool("isAlive", true);
            this.GetComponent<BoxCollider2D>().isTrigger = false;
            currentLife = maxLife;
        }
        gameController.GetComponent<Game_Controller>().SetGameState("runing");
    }

    public int GetCurrentLife()
    {
        return currentLife;
    }

    public int GetMaxLife()
    {
        return maxLife;
    }

    public void SetCurrentLife(int newCurrentLife)
    {
        currentLife = newCurrentLife;
    }

    public void SetMaxLife(int newMaxLife)
    {
        maxLife = newMaxLife;
    }

    public void TakeDmg(int dmg)
    {
        if (Time.time > nextDmg)
        {
            nextDmg = Time.time + coolDown;
            if (currentLife - dmg > 0)
            {
                currentLife = currentLife - dmg;
            }
            else
            {
                currentLife = 0;
                goblinState = "mauvais";
                goblinImage.GetComponent<Animator>().SetBool("isAlive", false);
                this.GetComponent<BoxCollider2D>().isTrigger = true;
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Ennemi")
        {
            collision.gameObject.GetComponent<EnnemiController>().TakeDmg(dmg);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ennemi")
        {
            collision.gameObject.GetComponent<EnnemiController>().TakeDmg(dmg);
        }
    }
}
