﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestionController : MonoBehaviour
{

    private GameObject callingGameObject;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Reponse")
        {
            if (collision.transform.GetChild(0).tag == "BonneReponse")
            {
                switch (callingGameObject.tag)
                {
                    case "Base":
                        callingGameObject.GetComponent<BaseController>().SendReponse(true);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Tour":
                        callingGameObject.GetComponent<TourController>().SendReponse(true);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Mur":
                        callingGameObject.GetComponent<MurController>().SendReponse(true);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Goblin":
                        callingGameObject.GetComponent<GoblinController>().SendReponse(true);
                        Destroy(transform.parent.gameObject);
                        break;
                }
            }
            else
            {
                switch (callingGameObject.tag)
                {
                    case "Base":
                        callingGameObject.GetComponent<BaseController>().SendReponse(false);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Tour":
                        callingGameObject.GetComponent<TourController>().SendReponse(false);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Mur":
                        callingGameObject.GetComponent<MurController>().SendReponse(false);
                        Destroy(transform.parent.gameObject);
                        break;
                    case "Goblin":
                        callingGameObject.GetComponent<GoblinController>().SendReponse(false);
                        Destroy(transform.parent.gameObject);
                        break;
                }
            }
        }
        
    }

    public void SetCallingObject(GameObject newCallingObject)
    {
        callingGameObject = newCallingObject;
        int rand = 0;
        switch (callingGameObject.tag)
        {
            case "Base":
                transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "<head>\n    < !--Titre de la page web-->\n    < title > Base </ title >\n    < !--Encodage de la page-- >\n    < meta charset = \"UTF-8\" />\n    < !--Lien au css(cascading styel sheet)-->\n    < link rel = StyleSheet href = \"CSS/style.css\" type = \"text/css\" >\n<......... > ";
                rand = Random.Range(0, 3);
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "/head";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "BonneReponse";
                while(transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "head";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "/html";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                break;
            case "Tour":
                transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "< !--Titre-->\n<.....> tour </ h1 >";
                rand = Random.Range(0, 3);
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "h1";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "BonneReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "h2";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "/h1";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                break;
            case "Mur":
                transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "< !--Lien-->\n < a .... = \"https://mur.com\" > Mur </ a >";
                rand = Random.Range(0, 3);
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "href ";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "BonneReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "ref ";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "src ";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                break;
            case "Goblin":
                transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "< !--Paragraphe-->\n< p ..... > goblin </ p >";
                rand = Random.Range(0, 3);
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "name=\"goblin\" ";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "BonneReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "name : \"goblin\" ";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                while (transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text != "New Text")
                {
                    rand = Random.Range(0, 3);
                }
                transform.parent.GetChild(rand + 1).GetChild(0).GetComponent<TextMeshProUGUI>().text = "name -> \"goblin\"";
                transform.parent.GetChild(rand + 1).GetChild(0).tag = "MauvaiseReponse";
                break;
        }
    }
}
