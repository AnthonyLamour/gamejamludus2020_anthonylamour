﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Controller : MonoBehaviour
{
    private string gameState;

    public void SetGameState(string newGameState)
    {
        gameState = newGameState;
    }

    public string GetGameState()
    {
        return gameState;
    }
}
