﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{

    public GameObject ennemi;

    private float nextSpawn;
    private float coolDown;

    private float maxCoolDown;
    private int cptEnnemi;

    // Start is called before the first frame update
    void Start()
    {
        nextSpawn = Time.time;
        maxCoolDown = 5f;
        coolDown = Random.Range(0.5f, maxCoolDown + 0.1f);
        cptEnnemi = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn && cptEnnemi<5)
        {
            Instantiate(ennemi, transform);
            cptEnnemi++;
            if (maxCoolDown > 0.6f)
            {
                maxCoolDown = maxCoolDown - 0.1f;
            }
            coolDown = Random.Range(0.5f, maxCoolDown + 0.1f);
            nextSpawn = Time.time + coolDown;
        }
    }

    public void DeadEnnemi()
    {
        cptEnnemi--;
    }
}
