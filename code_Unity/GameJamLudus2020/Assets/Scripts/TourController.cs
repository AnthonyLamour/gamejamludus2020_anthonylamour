﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourController : MonoBehaviour
{
    public Sprite deadTour;
    public Sprite aliveTour;
    public GameObject quiz;
    public float range;
    public GameObject bullet;
    public float cooldown;

    private string tourState;
    private GameObject gameController;
    private GameObject theQuiz;
    private Transform target;
    private float nextFire;

    private void Start()
    {
        tourState = "mauvais";
        transform.GetChild(0).GetComponent<Image>().sprite = deadTour;
        this.gameObject.GetComponent<Button>().onClick.AddListener(RepairBase);
        gameController = GameObject.FindGameObjectWithTag("GameController");
        nextFire = Time.time;
    }

    private void Update()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Ennemi");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }

        if(target!=null & Time.time > nextFire)
        {
            /*ne fonctionne pas
            nextFire = Time.time + cooldown;
            GameObject currentBullet= Instantiate(bullet, transform.parent);
            currentBullet.transform.LookAt(target);*/
        }
    }

    private void RepairBase()
    {
        if (tourState == "mauvais")
        {

            gameController.GetComponent<Game_Controller>().SetGameState("pause");
            theQuiz = Instantiate(quiz, transform.parent.transform);
            theQuiz.transform.GetChild(0).gameObject.GetComponent<QuestionController>().SetCallingObject(this.gameObject);
        }
    }

    public void SendReponse(bool bonneReponse)
    {
        if (bonneReponse)
        {
            tourState = "bon";
            transform.GetChild(0).GetComponent<Image>().sprite = aliveTour;
        }
        gameController.GetComponent<Game_Controller>().SetGameState("runing");
    }

}