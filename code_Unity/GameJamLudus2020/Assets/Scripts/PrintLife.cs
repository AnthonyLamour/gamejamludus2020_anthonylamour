﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintLife : MonoBehaviour
{
    public GameObject target;
    public Color lifeFull;
    public Color lifeGood;
    public Color lifeMiddle;
    public Color lifeBad;
    private string carText;
    private float maxBarWidth;
    private float widthBar;
    private float lifePercent;
    private bool dead;

    // Start is called before the first frame update
    void Start()
    {
        widthBar = transform.gameObject.GetComponent<RectTransform>().rect.width;

        maxBarWidth = widthBar;

        dead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            switch (target.tag)
            {
                case "Goblin":
                    widthBar = (target.GetComponent<GoblinController>().GetCurrentLife() * maxBarWidth) / target.GetComponent<GoblinController>().GetMaxLife();
                    break;
                case "Mur":
                    widthBar = (target.GetComponent<MurController>().GetCurrentLife() * maxBarWidth) / target.GetComponent<MurController>().GetMaxLife();
                    break;
                case "Base":
                    widthBar = (target.GetComponent<BaseController>().GetCurrentLife() * maxBarWidth) / target.GetComponent<BaseController>().GetMaxLife();
                    break;
                case "Ennemi":
                    widthBar = (target.GetComponent<EnnemiController>().GetCurrentLife() * maxBarWidth) / target.GetComponent<EnnemiController>().GetMaxLife();
                    break;
            }
            this.GetComponent<RectTransform>().offsetMax = new Vector2(-(maxBarWidth - widthBar), this.GetComponent<RectTransform>().offsetMax.y);

            lifePercent = (widthBar * 100) / maxBarWidth;
            if (lifePercent > 75)
            {
                this.GetComponent<Image>().color = lifeFull;
            }
            else if (lifePercent > 50)
            {
                this.GetComponent<Image>().color = lifeGood;
            }
            else if (lifePercent > 25)
            {
                this.GetComponent<Image>().color = lifeMiddle;
            }
            else
            {
                this.GetComponent<Image>().color = lifeBad;
            }
        }

    }
}
