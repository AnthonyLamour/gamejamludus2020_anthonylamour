﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnnemiController : MonoBehaviour
{

    public float ennemiSpeed;
    public int dmg;
    public int maxLife;
    private GameObject gameController;
    private int currentLife;

    public float coolDown;
    private float nextDmg;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
        currentLife = maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameController.GetComponent<Game_Controller>().GetGameState() != "pause")
        {
            //récupération du recttransform de la gameZone
            var ennemiRect = this.GetComponent<RectTransform>();
            //récupération de la collider 2D de la gameZone
            var ennemiCollider = this.GetComponent<BoxCollider2D>();

            //reset de l'offset de la collider
            ennemiCollider.offset = new Vector2(0, 0);
            //resize de la collider
            ennemiCollider.size = new Vector2(ennemiRect.rect.width + 10, ennemiRect.rect.height + 10);

            this.GetComponent<Rigidbody2D>().velocity = new Vector2(ennemiSpeed, 0.0f);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, 0.0f);
        }
        
    }

     private void OnCollisionEnter2D(Collision2D collision)
     {
         switch (collision.gameObject.tag)
         {
             case "Base":
                 collision.gameObject.GetComponent<BaseController>().TakeDmg(dmg);
                 break;
             case "Mur":
                 collision.gameObject.GetComponent<MurController>().TakeDmg(dmg);
                 break;
             case "Goblin":
                 collision.gameObject.GetComponent<GoblinController>().TakeDmg(dmg);
                 break;
         }
     }

    private void OnCollisionStay2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Base":
                collision.gameObject.GetComponent<BaseController>().TakeDmg(dmg);
                break;
            case "Mur":
                collision.gameObject.GetComponent<MurController>().TakeDmg(dmg);
                break;
            case "Goblin":
                collision.gameObject.GetComponent<GoblinController>().TakeDmg(dmg);
                break;
        }
    }

    public int GetCurrentLife()
    {
        return currentLife;
    }

    public int GetMaxLife()
    {
        return maxLife;
    }

    public void SetCurrentLife(int newCurrentLife)
    {
        currentLife = newCurrentLife;
    }

    public void SetMaxLife(int newMaxLife)
    {
        maxLife = newMaxLife;
    }

    public void TakeDmg(int dmg)
    {
        if (Time.time > nextDmg)
        {
            nextDmg = Time.time + coolDown;
            if (currentLife - dmg > 0)
            {
                currentLife = currentLife - dmg;
            }
            else
            {
                currentLife = 0;
                transform.parent.gameObject.GetComponent<SpawnerController>().DeadEnnemi();
                Destroy(this.gameObject);
            }
        }
    }
}
