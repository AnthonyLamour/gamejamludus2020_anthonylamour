﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MurController : MonoBehaviour
{
    public Sprite deadMur;
    public Sprite aliveMur;
    public int maxLife;
    public GameObject quiz;

    private string murState;
    private GameObject gameController;
    private GameObject theQuiz;
    private int currentLife;

    public float coolDown;
    private float nextDmg;

    private void Start()
    {
        murState = "mauvais";
        transform.GetChild(0).GetComponent<Image>().sprite = deadMur;
        this.gameObject.GetComponent<Button>().onClick.AddListener(RepairBase);
        gameController = GameObject.FindGameObjectWithTag("GameController");
        this.GetComponent<BoxCollider2D>().isTrigger = true;
    }

    private void RepairBase()
    {
        if (murState == "mauvais")
        {

            gameController.GetComponent<Game_Controller>().SetGameState("pause");
            theQuiz = Instantiate(quiz, transform.parent.transform);
            theQuiz.transform.GetChild(0).gameObject.GetComponent<QuestionController>().SetCallingObject(this.gameObject);
        }
    }

    public void SendReponse(bool bonneReponse)
    {
        if (bonneReponse)
        {
            murState = "bon";
            transform.GetChild(0).GetComponent<Image>().sprite = aliveMur;
            this.GetComponent<BoxCollider2D>().isTrigger = false;
            currentLife = maxLife;
        }
        gameController.GetComponent<Game_Controller>().SetGameState("runing");
    }

    public int GetCurrentLife()
    {
        return currentLife;
    }

    public int GetMaxLife()
    {
        return maxLife;
    }

    public void SetCurrentLife(int newCurrentLife)
    {
        currentLife = newCurrentLife;
    }

    public void SetMaxLife(int newMaxLife)
    {
        maxLife = newMaxLife;
    }

    public void TakeDmg(int dmg)
    {
        if (Time.time > nextDmg)
        {
            nextDmg = Time.time + coolDown;
            if (currentLife - dmg > 0)
            {
                currentLife = currentLife - dmg;
            }
            else
            {
                currentLife = 0;
                murState = "mauvais";
                transform.GetChild(0).GetComponent<Image>().sprite = deadMur;
                this.GetComponent<BoxCollider2D>().isTrigger = true;
            }
        }
    }
}
