﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    public float bulletSpeed;
    public int dmg;
    private Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        moveDirection = (transform.forward + transform.right);

        //apply speed to movement
        //FR application de la vitesse aux mouvements
        moveDirection = moveDirection.normalized * bulletSpeed;

        transform.position = transform.position + moveDirection;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ennemi")
        {
            collision.gameObject.GetComponent<EnnemiController>().TakeDmg(dmg);
            Destroy(this.gameObject);
        }
    }
}
