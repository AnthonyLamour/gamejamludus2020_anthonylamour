﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    //nom de la scène
    public string SceneName;

    //LoadMyScene 
    public void LoadMyScene()
    {
        //chargement le scène
        SceneManager.LoadScene(SceneName);
    }
}
